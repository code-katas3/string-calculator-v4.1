﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Calculator
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            string[] delimiters = GetDelimitersFrom(numbers);
            string[] strings = numbers.Split(delimiters,StringSplitOptions.None);
            List<int> numberList = new List<int>();
            foreach(var s in strings)
            {
                if(int.TryParse(s, out int number))
                {
                    numberList.Add(number);
                }
            }
              negativeNumberCheck(numberList);
              IgnoreNumbersGreaterThan1000(numberList);
              return numberList.Sum();
        }
        public void negativeNumberCheck(List<int> numberList)
        {    
             var negativeNumbers =numberList.Where(x => x < 0).ToList();
             if (negativeNumbers.Any())
                {
                    throw new Exception("Negatives not allowed : " + string.Join(" , ", negativeNumbers));
                }
        }
        public void IgnoreNumbersGreaterThan1000(List<int> numberList)
        {
            var maxNumber = 1000;
            numberList.RemoveAll(x => x > maxNumber);
        }
        public string[] getDelimiter(string numbers)
        { 
            if(!numbers.StartsWith("//"))
            {
                return new[] {",","\n"};
            }
            string[] delimiter;
            if(numbers.Contains('['))
            {
                int delimiterIndex = numbers.LastIndexOf(']');
                delimiter = numbers.Substring(2, delimiterIndex - 2).Split(new[] {"]["},StringSplitOptions.None);
                numbers = numbers.Substring(delimiterIndex + 1);
            }
            else
            {
                delimiter = new[]{numbers[2].ToString()};
                numbers = numbers.Substring(4);
            }
            return delimiter;

        }
       // public string[] getDelimiters(string)



     public string[] GetDelimitersFrom(string numbers)
        {
            if(NoDelimiterDataExistsIn(numbers))
            {
                 return new string[] { ",", "\n" };

            }
               
            int delimiterDataLength = numbers.IndexOf("\n") - 2;

            var delimiterData = numbers.Substring(2, delimiterDataLength);

            if(delimiterData.Contains("[") || delimiterData.Contains("]"))
            {
                string DelimiterListSeperator = "][";
     

                delimiterData = delimiterData.Substring(1, delimiterData.Length - 2);

                return delimiterData.Split(new string[] { DelimiterListSeperator }, StringSplitOptions.RemoveEmptyEntries);
            }

            var delimiterString = numbers.Substring(2, 1);

            return new string[] { delimiterString };
        }
          private bool NoDelimiterDataExistsIn(string numbers)
        {
            return !numbers.StartsWith("//");
        }
    }
}
