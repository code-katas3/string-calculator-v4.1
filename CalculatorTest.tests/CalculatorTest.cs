using NUnit.Framework;
using System;
using static System.Exception;

namespace Calculator
{
    public class CalculatorTest
    {
        [Test]
        [TestCase(0,"")]
        public void GIVEN_NullorEmptyString_WHEN_AddingNumbers_RETURN_Zero(int expected , string numbers)
        {
            
             var calc = new StringCalculator();
            
             var result = calc.Add(numbers);
             
             Assert.AreEqual(expected,result);
        }

        [Test]
        [TestCase(1,"1")]
        [TestCase(2,"2")]
         public void GIVEN_SingleNumber_WHEN_AddingNumbers_RETURN_ThatNumber(int expected , string numbers)
        {
             
             var calc = new StringCalculator();
            
             var result = calc.Add(numbers);
            
             Assert.AreEqual(expected,result);
        }

        [Test]
        [TestCase(3,"1,2")]
        public void GIVEN_TwoNumbers_WHEN_AddingStringNumbers_RETURN_SumOfNumbers(int expected , string numbers)
        {
             
             var calc = new StringCalculator();
            
             var result = calc.Add(numbers);
             
             Assert.AreEqual(expected,result);
        }

        [Test]
        [TestCase(29,"1,2,7,3,6,9,1")] 
        [TestCase(11,"1,1,2,3,4")] 
         public void GIVEN_MoreThanTwoNumbers_WHEN_AddingUnknownAmountOfNumbers_RETURN_SumOfNumbers(int expected , string numbers)
        {
            
             var calc = new StringCalculator();
            
             var result = calc.Add(numbers);
            
             Assert.AreEqual(expected,result);
        }

        [Test]
        [TestCase(6,"1\n2,3")]
        public void GIVEN_NewLinesSeparators_WHEN_AddingNumbers_RETURN_SumOfNumbers(int expected,string numbers)
        {
           
             var calc = new StringCalculator();
             
             var result = calc.Add(numbers);
            
             Assert.AreEqual(expected,result);
        }

        [Test]
        [TestCase(3,"//;\n1;2")]
        public void GIVEN_CustomDelimiters_WHEN_AddingNumbers_RETURN_SumOfNumbers(int expected, string numbers)
        {
            
             var calc = new StringCalculator();
             
             var result = calc.Add(numbers);
             
             Assert.AreEqual(expected,result);
        }

        [Test]
        [TestCase("-2","1,-2")]
        public static void GIVEN_NegativeNumbers_WHEN_AddingNumbers_RETURN_ThrowException(string expectedNegatives,string input)
        {
            
            var calc = new StringCalculator();
            
            var exception =Assert.Throws<Exception>(()=>calc.Add(input));
            
            Assert.AreEqual("Negatives not allowed : " + expectedNegatives,exception.Message);
        }

        [Test]
        [TestCase(2,"2,1001")]
        public void GIVEN_NumbersAbove1000_WHEN_AddingTwoPlus1001_RETURN_2(int expected,string input)
        {
            
             var calc = new StringCalculator();
             
             var result = calc.Add(input);
             
             Assert.AreEqual(expected,result);

        }

        [Test]
        [TestCase(6,"//***\n1***2***3")]
        public void GIVEN_CustomDelimiter_WHEN_AddingNumbers_RETURN_Sum(int expected,string numbers)
        {

             var calc = new StringCalculator();
            
             var result = calc.Add(numbers);
             
             Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(6,"//[*][%]\n1*2%3")]
        public void GIVEN_MultipleCustomDelimiterLenghtMoreThan1_WHEN_AddingNumbers_RETURN_Sum(int expected,string numbers)
        {
             
             var calc = new StringCalculator();
             
             var result = calc.Add(numbers);
            
             Assert.AreEqual(expected, result);
        }
    }
}